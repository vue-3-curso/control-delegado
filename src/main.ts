import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap"
import VueLazyLoad from 'vue3-lazyload'


const app=createApp(App)
app.use(router)
//las opciones las utilizaremos de manera explícita.
app.use(VueLazyLoad,{})
app.mount('#app')
